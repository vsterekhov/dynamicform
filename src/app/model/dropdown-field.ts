import { Field } from './field';

export class DropdownField extends Field<string> {
    controlType = 'dropdown';
    options: {key: string, value: string}[] = [];

    constructor(options: {} = {}) {
        super(options);
        this.options = Array.isArray(options['options']) ?
            options['options']
                .filter(option => option.hasOwnProperty('key') && option.hasOwnProperty('value'))
                .filter(option => typeof option.key === 'string' && typeof option.value === 'string') :
            [];
    }
}
