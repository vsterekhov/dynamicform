import { Field } from './field';
import { TextField } from './text-field';
import { DropdownField } from './dropdown-field';
import { RadiobuttonField } from './radiobutton-field';

export class Block {
    readonly key: string;
    readonly label?: string;
    readonly required: boolean;
    readonly order: number;
    readonly fields: Field<any>[];
    readonly children: Block[];

    constructor(options: {
        key?: string,
        label?: string,
        required?: boolean,
        order?: number,
        fields?: any[],
        children?: any[]
      } = {}) {
      this.key = options.key || '';
      this.label = options.label || '';
      this.required = !!options.required;
      this.order = options.order ? options.order : 1;

      this.fields = Array.isArray(options.fields) ? options.fields
        .filter(field => field.hasOwnProperty('controlType'))
        .map(field => {
            if (this.required) {
                field['required'] = true;
            }
            return field;
        })
        .map(field => {
            switch (field.controlType) {
                case 'textbox':
                    return new TextField(field);
                case 'dropdown':
                    return new DropdownField(field);
                case 'radiobutton':
                    return new RadiobuttonField(field);
                default: return null;
            }
        })
        .filter(field => field != null)
        .sort((a, b) => a.order - b.order) : [];

      this.children = Array.isArray(options.children) ? options.children
        .sort((a, b) => a.order - b.order)
        .map(child => {
            if (this.required) {
                child['required'] = true;
            }
            return child;
        })
        .map(child => new Block(child)) : [];
    }

}
