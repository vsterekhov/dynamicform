import { Field } from './field';

export class RadiobuttonField extends Field<string> {
    controlType = 'radiobutton';
    options: string[] = [];

    constructor(options: {} = {}) {
        super(options);
        this.options = Array.isArray(options['options']) ?
            options['options'].filter(option => typeof option === 'string') :
            [];
    }
}
