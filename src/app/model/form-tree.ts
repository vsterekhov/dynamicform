import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Block } from './block';
import { DropdownField } from './dropdown-field';
import { RadiobuttonField } from './radiobutton-field';

export class FormTree {
    readonly root: FormGroup;

    constructor(root: FormGroup) {
        this.root = root;
    }

    static map(block: Block, tree: FormTree = null, node: FormGroup = null): FormTree {
        let group: any = {};

        block.fields
        .map(field => {
            if ( (field instanceof DropdownField || field instanceof RadiobuttonField) &&
                field.options.length === 0) {

                field.required = false;
            }
            return field;
        })
        .forEach(field => {
            group[field.key] = field.required ?
                new FormControl(field.value || '', Validators.required) :
                new FormControl(field.value || '');
        });

        const formGroup = new FormGroup(group);

        if (tree === null) {
            tree = new FormTree(formGroup);
            node = tree.root;
        } else {
            node.addControl(block.key, formGroup);
        }

        block.children.forEach(child => FormTree.map(child, tree, formGroup));

        return tree;
    }
}
