import { Component } from '@angular/core';
import { ConfigService } from './service/config.service';
import { Block } from './model/block';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  template: `<app-dynamic-form *ngIf="block$ | async as block" [block]="block"></app-dynamic-form>`
})
export class AppComponent {
  public block$: Observable<Block>;

  constructor(private config: ConfigService) {
    this.block$ = this.config.getConfig();
  }
}
