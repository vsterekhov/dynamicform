import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { map, takeUntil } from 'rxjs/operators';
import { Field } from '../../model/field';
import { Subject } from 'rxjs';
import { TextField } from 'src/app/model/text-field';

@Component({
  selector: 'app-dynamic-form-field',
  templateUrl: './dynamic-form-field.component.html',
  styleUrls: ['./dynamic-form-field.component.scss']
})
export class DynamicFormFieldComponent implements OnInit, OnDestroy {
  @Input() field: Field<any>;
  @Input() formGroup: FormGroup;

  public textFieldPlaceholder = 'Any Text';
  private destroyed$ = new Subject<void>();

  ngOnInit() {
    if (this.field instanceof TextField && this.field.type === 'number') {
      this.textFieldPlaceholder = 'Only numbers';
      const formControl: FormControl = this.formGroup.get(this.field.key) as FormControl;

      formControl.valueChanges
      .pipe(
        map(value => value.replace(/\D/g, '')),
        takeUntil(this.destroyed$)
      )
      .subscribe(value => {
        formControl.setValue(value, {emitEvent: false});
      });
    }
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}
