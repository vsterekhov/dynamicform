import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormService } from 'src/app/service/form.service';
import { Block } from 'src/app/model/block';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit {

  @Input() block: Block;
  form: FormGroup;
  payLoad = '';

  constructor(private formService: FormService) { }

  ngOnInit() {
    this.form = this.formService.toForm(this.block);
  }

  onSubmit() {
     this.payLoad = JSON.stringify(this.form.value, null, 4);
  }

}
