import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Block } from '../model/block';
import { FormTree } from '../model/form-tree';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  toForm(block: Block): FormGroup {
    return FormTree.map(block).root;
  }

}
