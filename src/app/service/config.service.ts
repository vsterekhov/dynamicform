import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Block } from '../model/block';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  configUrl = 'assets/config.json';

  constructor(private http: HttpClient) { }

  getConfig(): Observable<Block> {
    return this.http.get(this.configUrl, {responseType: 'text'})
      .pipe(
        map((response) => {
          return JSON.parse(response, (key, value) => {
            if (key === 'order') {
              return parseInt(value, 10);
            }
            return value;
          });
       }),
       map(options => new Block(options))
      );
  }

}
