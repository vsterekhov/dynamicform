# Dynamicform

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.21.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. 

## Описание задачи 

#### Фронтенд на Angular и Material UI, для создания динамических форм ввода данных. Конфигурация формы ввода задается через json файл.

Форма ввода состоит из набора блоков.

Каждый блок состоит из дочерних блоков и полей. Любая глубина вложенности.

Поля и блоки могут быть помечены как обязательные для заполнения.

Поля могут быть следующих типов: 

    1. Строковое

    2. Числовое

    3. Radiobutton из нескольких значений

    4. DropDown из нескольких значений

Пользователь заполняет форму и отправляет(просто имитация отправки) данные на сервер в виде json.

## Общее описание реализации

Конфигурация формы хранится в файле `config.json` в папке `assets`. Конфигурация представляет из себя вложенный объект: объект верхнего уровня - корневой обект, - у которого есть свойство `children`, представляющее собой массив объектов, аналогичных корневому, у которых, в свою очердь, тоже может быть свойство `children` и т.д.

Каждый такой объект представляет отдельный блок формы. Корневой объект представляет саму форму, но по сути её тоже можно считать блоком.

Каждый объект, помимо свойства `children`, может иметь свойство `fields` - это массив объектов, описывающих поля ввода данного блока. Поля не имеют вложенности - то есть объект, описывающий одно поле, не может быть вложен в объект, описывающий другое поле. 
Вложенность имеют только блоки.

В приложение json с конфигурацией загружается через сервис `ConfigService`, который преобразует json-объект в объект класса `Block`. Объект класса `Block` представляет в приложении конфигурацию формы - он так же, как и json-объект, имеет свойство `children`, содежащее массив вложенных объектов класса `Block`, так же может иметь свойство `fields` и т.д.

Полученный от `ConfigService` объект класса `Block` передаётся в компонент `app-dynamic-form`. Этот компонент отвечает за отображение формы. В `app-dynamic-form` вызывается сервис `FormService`, которому передаётся полученный объект класса `Block`. `FormService`, в свою очередь, вызывает класс `FormTree`, который, на основании объекта `Block`, создаёт реактивную форму. 

В конечном итоге в `app-dynamic-form` из `FormService` возвращается реактивная форма, которая привязывается к шаблону этого компонента.
Каждое поле ввода формы в `app-dynamic-form` представляется компонентом `app-dynamic-form-field`.

## Некоторые детали

Как поля, так и блоки имеют свойство `order` - числовое свойство, задающее порядок вывода элементов. Во время преобразования json-объекта конфигурации в объект класса `Block`, происходит сортировка всех массивов `children` и `fields` по этому свойству.

Поля и блоки имеют свойство `required`, помечающее элемент (поле или блок) как обязательный для заполнения. Если блок отмечен, как объязательный, то все его поля и поля всех его дочерних блоков на всю глубину вложености во время преобразования из json в `Block` будут отмечены как обязательные вне зависимости от того, как они были отмечены в объекте json. То есть если, например, корневой объект json отмечен как обязательный, то все поля формы автоматически будут обязательными.

Во время преобразования из json в `Block` любая непустая строка в свойстве `required` интерпретируется как признак обязательности, поэтому и `"required": "true"`, и `"required": "false"` сделают элемент обязательным. Чтобы пометить элемент, как необязательный, нужно либо оставить пустую строку в свойстве `required` в json, либо не указывать у элемента это свойство вообще, либо указать не строковое значение, а логическое значение `false`. Соответственно, чтобы сделать элемент обязательным, нужно указывать либо любую непустую строку, либо логическое значение `true`.

У объектов, описывающих поля ввода, есть свойства `controlType` и `value`. У блоков этих свойств нет. Через свойство `value` можно передать в форму предустановленное значение, которое будет отображено в соответствующем поле ввода. 

Свойство `controlType` является обязательным. Оно показывает какое из четырех, перечисленных выше, типов полей описывает объект. 
В данном случае оно может иметь одно из трёх значений: 

    1. textbox

    2. dropdown

    3. radiobutton

json-объекты, описывающие поля ввода, в приложении преобразуются в объекты одного из трёх классов. На основании этих трёх значений и делается выбор к какому классу нужно привести json-объект. Если не указать в json свойство `controlType`, то не понятно будте объект какого класса нужно создать и поле будет просто отброшено - то есть в итоговой форме его не будет. Поэтому свойство `controlType` у объектов полей ввода в json нужно указывать обязательно.

Типов полей ввода четыре, а вариантов значения свойства `controlType` всего три потому, что для строкового и числового поля используется один и тот же класс - `TextField` (поскольку в шаблоне эти поля представляются одинаковыми элементами разметки).

Чтобы отличать числовое поле от строкового, в классе `TextField` введено дополнительное свойство `type`. У числового поля оно должно содержать строкоу `number` (у строкового поля оно ничего не содержит). Соответственно в json-объекте числового поля нужно указывать свойство `type` со значением `number`.

В случае числвого поля, при создании элемента разметки для него, на этот элемент добавляется обработчик события, который запрещает ввод либо вставку через copy-paste любых символов, кроме числовых.

При нажатии кнопки `Submit` json-объект с результатом отображается ниже кнопки на этой же странице.

